﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    public class Math<T> where T : IComparable<T>
    {
        /// <summary>
        /// Возвращает минимум на заданном массиве чисел
        /// </summary>
        /// <param name="Numbers"></param>
        /// <returns>min</returns>
        public static T Min(IEnumerable<T> Numbers)
        {
            if (Numbers.Count<T>() == 0)
            {
                return default(T);
            }
            else
            {
                T min = Numbers.ElementAt(0);
                foreach (T nam in Numbers)
                    if (nam.CompareTo(min) < 0)
                        min = nam;
                return min;
            }
        }
        /// <summary>
        /// Возвращает максимум на заданном массиве чисел
        /// </summary>
        /// <param name="Numbers"></param>
        /// <returns>max</returns>
        public static T Max(IEnumerable<T> Numbers)
        {
            if (Numbers.Count<T>() == 0)
            {
                return default(T);
            }
            else
            {
                T max = Numbers.ElementAt(0);
                foreach (T nam in Numbers)
                    if (nam.CompareTo(max) > 0)
                        max = nam;
                return max;
            }
        }
        /// <summary>
        /// Возвращает сумму чисел заданного массива
        /// </summary>
        /// <param name="Numbers"></param>
        /// <returns>sum</returns>
        public static double Sum(double[] Numbers)
        {
            double sum = 0;
            foreach (var nam in Numbers)
                sum+=nam;
            return sum;
        }
    }
}
