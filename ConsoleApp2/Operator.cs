﻿namespace Calculator
{
    public class Operator
    {
        /// <summary>
        /// Сложение двух чисел с плавающей точкой
        /// </summary>
        /// <param name="one"></param>
        /// <param name="two"></param>
        /// <returns>one+two</returns>
        public static double Add(double one, double two)
        {
            return one + two;
        }
        /// <summary>
        /// Вычитание второго числа из первого (Числа с плавающей точкой)
        /// </summary>
        /// <param name="one"></param>
        /// <param name="two"></param>
        /// <returns>one-two</returns>
        public static double Deduc(double one, double two)
        {
            return one - two;
        }
        /// <summary>
        /// Умножение двух чисел с плавающей точкой
        /// </summary>
        /// <param name="one"></param>
        /// <param name="two"></param>
        /// <returns>one*two</returns>
        public static double Mult(double one, double two)
        {
            return one * two;
        }
        /// <summary>
        /// Деление первого числа с плава
        /// </summary>
        /// <param name="one"></param>
        /// <param name="two"></param>
        /// <returns>one/two</returns>
        public static double Div(double one, double two)
        {
            return one / two;
        }
    }
}
