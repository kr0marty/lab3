﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Calculator;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Test_Add()
        {
            for (int i=0; i<100; i++)
            { 
                Random rand = new Random();
                double x = rand.NextDouble() * 1000000;
                double y = rand.NextDouble() * 1000000;
                double result = Operator.Add(x, y);
                Assert.AreEqual(result, x + y);
            }
        }
        [TestMethod]
        public void Test_Deduc()
        {
            for (int i = 0; i < 100; i++)
            {
                Random rand = new Random();
                double x = rand.NextDouble() * 1000000;
                double y = rand.NextDouble() * 1000000;
                double result = Operator.Deduc(x, y);
                Assert.AreEqual(result, x - y);
            }
        }
        [TestMethod]
        public void Test_Mult()
        {
            for (int i = 0; i < 100; i++)
            {
                Random rand = new Random();
                double x = rand.NextDouble() * 1000000;
                double y = rand.NextDouble() * 1000000;
                double result = Operator.Mult(x, y);
                Assert.AreEqual(result, x * y);
            }
        }
        [TestMethod]
        public void Test_Div()
        {
            for (int i = 0; i < 100; i++)
            {
                Random rand = new Random();
                double x = rand.NextDouble() * 1000000;
                double y = rand.NextDouble() * 1000000;
                double result = Operator.Div(x, y);
                Assert.AreEqual(result, x / y);
            }
        }
        [TestMethod]
        public void Test_IsMax_For_Int()
        {
            for (int i = 0; i < 100; i++)
            {
                Random rand = new Random();
                int one = rand.Next();
                int two = rand.Next();
                int result = Compare<int>.Max(one, two);
                if (one > two) Assert.AreEqual(result, 1);
                else if (one == two) Assert.AreEqual(result, 0);
                else Assert.AreEqual(result, 2);
            }
        }
        [TestMethod]
        public void Test_IsMax_For_double()
        {
            for (int i = 0; i < 100; i++)
            {
                Random rand = new Random();
                double one = rand.NextDouble() * 1000000;
                double two = rand.NextDouble() * 1000000;
                int result = Compare<double>.Max(one, two);
                if (one > two) Assert.AreEqual(result, 1);
                else if (one == two) Assert.AreEqual(result, 0);
                else Assert.AreEqual(result, 2);
            }
        }
        [TestMethod]
        public void Test_IsMin_For_Int()
        {
            for (int i = 0; i < 100; i++)
            {
                Random rand = new Random();
                int one = rand.Next();
                int two = rand.Next();
                int result = Compare<int>.Min(one, two);
                if (one > two) Assert.AreEqual(result, 2);
                else if (one == two) Assert.AreEqual(result, 0);
                else Assert.AreEqual(result, 1);
            }
        }
        [TestMethod]
        public void Test_IsMin_For_Double()
        {
            for (int i = 0; i < 100; i++)
            {
                Random rand = new Random();
                double one = rand.NextDouble() * 1000000;
                double two = rand.NextDouble() * 1000000;
                int result = Compare<double>.Min(one, two);
                if (one > two) Assert.AreEqual(result, 2);
                else if (one == two) Assert.AreEqual(result, 0);
                else Assert.AreEqual(result, 1);
            }
        }
        [TestMethod]
        public void Test_IsEqual_For_Int()
        {
            Assert.IsTrue(Compare<int>.IsEqual(100, 100));
            for (int i = 0; i < 99; i++)
            {
                Random rand = new Random();
                int one = rand.Next();
                int two = rand.Next();
                bool result = Compare<int>.IsEqual(one, two);
                if (one == two) Assert.IsTrue(result);
                else Assert.IsFalse(result);
            }
        }
        [TestMethod]
        public void Test_IsEqual_For_Double()
        {
            Assert.IsTrue(Compare<double>.IsEqual(10.01, 10.01));
            for (int i = 0; i < 99; i++)
            {
                Random rand = new Random();
                double one = rand.NextDouble() * 1000000;
                double two = rand.NextDouble() * 1000000;
                bool result = Compare<double>.IsEqual(one, two);
                if (one == two) Assert.IsTrue(result);
                else Assert.IsFalse(result);
            }
        }
        [TestMethod]
        public void Test_Max_For_Int()
        {
            for (int k = 0; k < 100; k++)
            {
                Random rand = new Random();
                int a = rand.Next() % 1000000;
                int[] array = new int[a];
                for (int i = 0; i < array.Length; i++)
                    array[i] = rand.Next();
                int result = Math<int>.Max(array);
                if (a > 0)
                {
                    int max = array[0];
                    foreach (var i in array)
                        max = Math.Max(i, max);
                    Assert.AreEqual(max, result);
                }
                else Assert.AreEqual(default(int), result);
            }
        }
        [TestMethod]
        public void Test_Max_For_Double()
        {
            for (int k = 0; k < 100; k++)
            {
                Random rand = new Random();
                int a = rand.Next() % 1000000;
                double[] array = new double[a];
                for (int i = 0; i < array.Length; i++)
                    array[i] = rand.NextDouble() * 1000000;
                double result = Math<double>.Max(array);
                if (a > 0)
                {
                    double max = array[0];
                    foreach (var i in array)
                        max = Math.Max(i, max);
                    Assert.AreEqual(max, result);
                }
                else Assert.AreEqual(default(double), result);
            }
        }
        [TestMethod]
        public void Test_Min_For_Int()
        {
            for (int k = 0; k < 100; k++)
            {
                Random rand = new Random();
                int a = rand.Next() % 1000000;
                int[] array = new int[a];
                for (int i = 0; i < array.Length; i++)
                    array[i] = rand.Next();
                int result = Math<int>.Min(array);
                if (a > 0)
                {
                    int min = array[0];
                    foreach (var i in array)
                        min = Math.Min(i, min);
                    Assert.AreEqual(min, result);
                }
                else Assert.AreEqual(default(int), result);
            }
        }
        [TestMethod]
        public void Test_Min_For_Double()
        {
            for (int k = 0; k < 100; k++)
            {
                Random rand = new Random();
                int a = rand.Next() % 1000000;
                double[] array = new double[a];
                for (int i = 0; i < array.Length; i++)
                    array[i] = rand.NextDouble() * 1000000;
                double result = Math<double>.Min(array);
                if (a > 0)
                {
                    double min = array[0];
                    foreach (var i in array)
                        min = Math.Min(i, min);
                    Assert.AreEqual(min, result);
                }
                else Assert.AreEqual(default(double), result);
            }
        }
        [TestMethod]
        public void Test_Sum()
        {
            for (int k = 0; k < 100; k++)
            {
                Random rand = new Random();
                int a = rand.Next() % 1000000;
                double[] array = new double[a];
                for (int i = 0; i < array.Length; i++)
                    array[i] = rand.NextDouble() * 1000000;
                    double sum = 0;
                foreach (var i in array)
                    sum += i;
                    Assert.AreEqual(sum, actual: Math<double>.Sum(array));
            }
        }
    }
}
